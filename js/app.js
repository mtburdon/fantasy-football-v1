'use strict';

var footyApp = angular.module('footyApp', []);

footyApp.factory('League', function() {
  var League = {};
  var imgPath = 'images/team-badges/';

  // Goals vars
  var titleContenders = [0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 6, 7];
  var topHalf = [0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 6];
  var midTable = [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 4];
  var relegationContenders = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3];

  League.teams = [
    {
      name: "Arsenal",
      image: imgPath + 'arsenal.png',
      goals: titleContenders,
      teamId: 1
    },
    {
      name: "Aston Villa",
      image: imgPath + 'aston-villa.png',
      goals: midTable,
      teamId: 2
    },
    {
      name: "Cardiff",
      image: imgPath + 'cardiff.png',
      goals: relegationContenders,
      teamId: 3
    },
    {
      name: "Chelsea",
      image: imgPath + 'chelsea.png',
      goals: titleContenders,
      teamId: 4
    },
    {
      name: "Crystal Palace",
      image: imgPath + 'crystal-palace.png',
      goals: relegationContenders,
      teamId: 5
    },
    {
      name: "Everton",
      image: imgPath + 'everton.png',
      goals: topHalf,
      teamId: 6
    },
    {
      name: "Fulham",
      image: imgPath + 'fulham.png',
      goals: midTable,
      teamId: 7
    },
    {
      name: "Hull",
      image: imgPath + 'hull.png',
      goals: relegationContenders,
      teamId: 8
    },
    {
      name: "Liverpool",
      image: imgPath + 'liverpool.png',
      goals: titleContenders,
      teamId: 9
    },
    {
      name: "Manchester City",
      image: imgPath + 'manchester-city.png',
      goals: titleContenders,
      teamId: 10
    },
    {
      name: "Manchester United",
      image: imgPath + 'manchester-united.png',
      goals: titleContenders,
      teamId: 11
    },
    {
      name: "Newcastle",
      image: imgPath + 'newcastle.png',
      goals: midTable,
      teamId: 12
    },
    {
      name: "Norwich",
      image: imgPath + 'norwich.png',
      goals: relegationContenders,
      teamId: 13
    },
    {
      name: "Southampton",
      image: imgPath + 'southampton.png',
      goals: topHalf,
      teamId: 14
    },
    {
      name: "Stoke",
      image: imgPath + 'stoke.png',
      goals: midTable,
      teamId: 15
    },
    {
      name: "Sunderland",
      image: imgPath + 'sunderland.png',
      goals: midTable,
      teamId: 16
    },
    {
      name: "Swansea",
      image: imgPath + 'swansea.png',
      goals: topHalf,
      teamId: 17
    },
    {
      name: "Tottenham",
      image: imgPath + 'tottenham.png',
      goals: topHalf,
      teamId: 18
    },
    {
      name: "West Brom",
      image: imgPath + 'west-brom.png',
      goals: topHalf,
      teamId: 19
    },
    {
      name: "West Ham",
      image: imgPath + 'west-ham.png',
      goals: midTable,
      teamId: 20
    }
  ];
  return League;
})