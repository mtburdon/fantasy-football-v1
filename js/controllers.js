'use strict';

footyApp.controller('FootyController',
  function FootyController($scope, League) {

    $scope.league = League; // This forms the league table
    $scope.user = {         // Some user info on team selected
      teamIndex: null,
      team: null,
      badge: null
    }
    $scope.fixtures = angular.copy($scope.league);  // A copy of the league minus team selected
    $scope.gameWeek = 1;

    $scope.selectTeam = function(team) {
      // Get some info from the clicked team and store in user info
      $scope.user.teamIndex = $scope.league.teams.indexOf(team);
      $scope.user.team = team.name;
      $scope.user.badge = team.image;

      // Get the index of the clicked team in the teams array
      var index = $scope.league.teams.indexOf(team);
      // And remove it from the fixtures array
      $scope.fixtures.teams.splice(index,1);

      // Add some extra data to each node in the fixtures and league arrays
      $scope.createFixtures();
      $scope.createLeagueTable();

      $scope.rr(6);
    }

    $scope.createFixtures = function() {
      // Add some new info to each array object
      var length = $scope.fixtures.teams.length,
        element = null;
      for (var i = 0; i < length; i++) {
        element = $scope.fixtures.teams[i];

        // Add some new data to the fixtures array
        element.userTeam = {
          team: $scope.user.team,
          badge: $scope.user.badge
        }
        element.gameInfo = {
          userScore: null,
          compScore: null,
          outcome: null
        }
      }
    }

    $scope.createLeagueTable = function() {
      // Add some new info to each array object
      var length = $scope.league.teams.length,
        element = null;
      for (var i = 0; i < length; i++) {
        element = $scope.league.teams[i];

        // element.goals = element.goals;

        // Add some new data to the league array
        element.tableData = {
          points: 0,
          goalsFor: 0,
          goalsConceded: 0
        }
      }
    }

    $scope.playGame = function() {
      var gameInfo = $scope.fixtures.teams[$scope.gameWeek - 1].gameInfo;
      var awayTeamIndex = $scope.fixtures.teams[$scope.gameWeek - 1].teamId;
      
      // Generate random numbers for the scores
      gameInfo.userScore = $scope.league.teams[$scope.user.teamIndex].goals[Math.floor(Math.random() * $scope.league.teams[$scope.user.teamIndex].goals.length)];
      gameInfo.compScore = $scope.league.teams[awayTeamIndex - 1].goals[Math.floor(Math.random() * $scope.league.teams[awayTeamIndex - 1].goals.length)];

      // Update the teams goals tally
      $scope.league.teams[$scope.user.teamIndex].tableData.goalsFor += gameInfo.userScore;
      $scope.league.teams[awayTeamIndex - 1].tableData.goalsFor += gameInfo.compScore;

      // Work out what the outcome was and pass in the position of the opposition team
      $scope.decideOutcome(gameInfo, awayTeamIndex);
      $scope.gameWeek++;
      $scope.simGameWeek(awayTeamIndex);
    }

    $scope.decideOutcome = function(gameInfo, awayTeamIndex) {
      if(gameInfo.userScore === gameInfo.compScore) {
        gameInfo.outcome = "draw";
        $scope.league.teams[$scope.user.teamIndex].tableData.points += 1;
        $scope.league.teams[awayTeamIndex - 1].tableData.points += 1;
      }
      else if(gameInfo.userScore > gameInfo.compScore) {
        gameInfo.outcome = "win";
        $scope.league.teams[$scope.user.teamIndex].tableData.points += 3;
      }
      else {
        gameInfo.outcome = "loss";
        $scope.league.teams[awayTeamIndex - 1].tableData.points += 3;
      }
    }

    $scope.simGameWeek = function(awayTeamIndex) {
      // 9 other games, so...
      // 5 wins (3 points to 5 different teams, 0 points to 5 different teams)
      // 4 draws (1 point to 8 teams)

      // Get a fresh copy of all the teams
      $scope.theTeams = angular.copy($scope.league);

      // Set our team and the opposition for the week to null in the array
      $scope.theTeams.teams[$scope.user.teamIndex] = null;
      $scope.theTeams.teams[awayTeamIndex - 1] = null;

      // Assign 5 random wins
      for (var i = 1; i <= 5; i++) {
        var id = $scope.random(0, 19);
        // Check to make sure this team isn't null / already 'played'
        if($scope.theTeams.teams[id] == null)
        {
          i--;
        } else {
          $scope.league.teams[id].tableData.points += 3;
          $scope.league.teams[id].tableData.goalsFor += $scope.random(1, 4);
          $scope.theTeams.teams[id] = null;
        }
      };

      // Assign 4 random draws
      for (var i = 1; i <= 8; i++) {
        var id = $scope.random(0, 19);
        // Check to make sure this team isn't null / already 'played'
        // console.log(id);
        if($scope.theTeams.teams[id] == null)
        {
          i--;
        } else {
          $scope.league.teams[id].tableData.points += 1;
          $scope.league.teams[id].tableData.goalsFor += $scope.random(1, 4);
          $scope.theTeams.teams[id] = null;
        }
      };
    }

    // This should be a service so can be used anywhere in app
    $scope.random = function(from,to) {
      return Math.floor(Math.random()*(to-from+1)+from);
    }

    $scope.rr = function(teams) {
      var  i;
      var  ret = "" ;
      var  round;
      var  numplayers = 0;
      numplayers = parseInt(teams) + parseInt(teams % 2);
      numplayers = parseInt(numplayers);
      var  a = new  Array(numplayers - 1);
      var  alength = a.length;
      for  (var  x = 0; x < (numplayers); x++) { a[x] = "Team "  + (x + 1); }
      if  (numplayers != parseInt(teams)) { a[alength] = "BYE" ; }
      var  pos;
      var  pos2;

      var fixtures = new Array();

      for(var i = 1; i <= alength; i++)
      {
        fixtures[i] = new Array();
      }

      ret = "----- ROUND #1-----<br />" 
console.log("ROUND 1");
// fixtures[1] = new Array();
      for  (var  r1a = 0; r1a < (numplayers / 2); r1a++) {
         ret += a[r1a] + " vs. "  + a[alength - r1a] + "<br />" 
console.dir(a[r1a] + " vs. "  + a[alength - r1a]);
var ob = {
  home: a[r1a],
  away: a[alength - r1a]
}
fixtures[1].push(ob);
      }
console.log("ROUND 2");
      for  (round = 2; round < alength + 1; round++) {
         ret += "<br /><br />----- ROUND #"  + round + "-----<br />" 
         ret += a[0] + " vs. "  + a[alength - (round - 1)] + "<br />" 

console.dir(round + a[0] + " vs. "  + a[alength - (round - 1)]);
fixtures[round].push(a[0] + " v " + a[alength - (round - 1)]);
         // fixtures[round] = new Array();
         // fixtures[round].push(a[0] + " v " + a[alength - (round - 1)]);
         
         for  (i = 2; i < (numplayers / 2) + 1; i++) {
             pos = (i + (round - 2))
             if  (pos >= alength) { pos = ((alength - pos)) * -1 }
             else 
             { pos = (i + (round - 2)) }

             pos2 = (pos - (round - 2)) - round
             if  (pos2 > 0) {
                 pos2 = (alength - pos2) * -1
             }

             if  (pos2 < (alength * -1)) {
                 pos2 += alength
             }

             ret += a[(alength + pos2)]
             ret += " vs. "  + a[(alength - pos)] + "<br />"
console.dir(round + a[(alength + pos2)] + " vs. " + a[(alength - pos)]);
fixtures[round].push(a[(alength + pos2)] + " v " + a[(alength - pos)]);
         }
console.log("ROUND " + (round + 1));
      }
      console.dir(fixtures);
      return  ret;
    }

  }
);